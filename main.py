import sys
import datetime

import cv2

from PyQt5.QtCore import QTimer
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtWidgets import QDialog, QApplication, QFileDialog, QMessageBox
from PyQt5.uic import loadUi


class Capture(QDialog):
    image_dir = 'images'

    def __init__(self):
        super(Capture, self).__init__()
        loadUi('cap.ui', self)
        self.image = ...
        self.btnStart.clicked.connect(self.start_webcam)
        self.btnStop.clicked.connect(self.stop_webcam)
        self.btnSave.clicked.connect(self.save_image)

    def start_webcam(self):
        self.capture = cv2.VideoCapture(0)

        self.capture.set(cv2.CAP_PROP_AUTO_EXPOSURE, 0.0)  # 自動露光解除(win7:0)
        print(self.capture.get(cv2.CAP_PROP_AUTO_EXPOSURE))
        self.capture.set(cv2.CAP_PROP_EXPOSURE, -4.0)  # 露光補正値（win7:-7.0～0.0くらいまでは確認した）
        print(self.capture.get(cv2.CAP_PROP_EXPOSURE))
        self.capture.set(cv2.CAP_PROP_FPS, 20.0)
        print(self.capture.get(cv2.CAP_PROP_FPS))

        self.capture.set(cv2.CAP_PROP_FRAME_HEIGHT, 960)
        self.capture.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)

        self.timer = QTimer(self)
        self.timer.timeout.connect(self.update_frame)
        # self.timer.start(5)
        self.timer.start(20)

    def update_frame(self):
        ret, self.image = self.capture.read()
        self.image = cv2.flip(self.image, 1)
        self.display_image(self.image, 1)

    def stop_webcam(self):
        self.timer.stop()

    def display_image(self, img, window=1):
        qformat = QImage.Format_Indexed8
        if len(img.shape) == 3:  # rows[0], cols[1], channels[2]
            if img.shape[2] == 4:
                qformat = QImage.Format_RGBA8888
            else:
                qformat = QImage.Format_RGB888
        out_image = QImage(img, img.shape[1], img.shape[0], img.strides[0], qformat)
        # BGR -> RGB
        out_image = out_image.rgbSwapped()

        if window == 1:
            self.lblImage.setPixmap(QPixmap.fromImage(out_image))
            self.lblImage.setScaledContents(True)

    def save_image(self):
        extension = 'png'
        now = datetime.datetime.now()
        # 現在時刻を織り込んだファイル名を生成
        file_name = 'image_{0:%Y%m%d-%H%M%S}.'.format(now) + extension
        cv2.imwrite(Capture.image_dir + '/' + file_name, self.image)


if __name__ == "__main__":
    # アプリケーション作成
    app = QApplication(sys.argv)
    # オブジェクト作成
    window = Capture()
    window.setWindowTitle('capture camera image')
    # MainWindowの表示
    window.show()
    # MainWindowの実行
    sys.exit(app.exec_())
